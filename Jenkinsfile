pipeline {
    agent none

    stages {

        stage('Clone') {
            agent any
            steps {
                git credentialsId: 'gitlab-fake-user',
                    url: 'https://gitlab.com/BenceDD/samplespringproject.git'
            }
        }

        stage('Build') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2'
                }
            }
            steps {
                sh 'mvn clean install'
            }
        }
    
        stage('Code quality check') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2 --network cicd_cicd-demo-network'
                }
            }
            steps {
                withSonarQubeEnv('local-sonarqube-server') { 
                    sh 'mvn sonar:sonar -Dsonar.projectKey=my-first-project'
                }
            }
        }
        
        stage("Quality Gate") {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        
        stage('Package deploy') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2 --network cicd_cicd-demo-network'
                }
            }
            steps {
                sh 'mvn --settings settings.xml deploy'
            }
        }

        stage('Docker build') {
            agent any
            steps {
                script {
                    def fileName = "demo-0.0.1-SNAPSHOT.jar"
                    docker.build("test-image-xx", "--build-arg WAR_NAME=${fileName} .")
                }
                
            }
        }

        stage ('Docker deploy') {
            agent any
            steps {
                rtDockerPush(
                    serverId: "devops-artifactory-server",
                    image: "https://devopsacademy.jfrog.io/artifactory/docker-local/test-image-xx:latest",
                    //host: "tcp://127.0.0.1:1234",
                    targetRepo: 'docker-local'
                )
            }
        }

    }
}
